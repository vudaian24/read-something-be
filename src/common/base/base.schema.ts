import { Prop } from '@nestjs/mongoose';
import { SchemaTypes, Types } from 'mongoose';

export class MongoBaseSchema {
    _id: Types.ObjectId;

    @Prop({ required: false, default: null, type: Number })
    createdAt: number;

    @Prop({ required: false, default: null, type: Number })
    updatedAt: number;

    @Prop({ required: false, default: null, type: Number })
    deletedAt?: number;

    // Convert Date to timestamp
    toTimestamp(date: Date | number): number {
        if (date instanceof Date) {
            return date.getTime();
        }
        return date;
    }

    toTimestamps(): void {
        this.createdAt = this.toTimestamp(this.createdAt);
        this.updatedAt = this.toTimestamp(this.updatedAt);
        if (this.deletedAt !== undefined) {
            this.deletedAt = this.toTimestamp(this.deletedAt);
        }
    }
}