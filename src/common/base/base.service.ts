import { Injectable } from '@nestjs/common';
import { FilterQuery, Model, Types } from 'mongoose';
import { toObjectId } from '../commonFunctions';

@Injectable()
export class BaseService<T extends MongoBaseSchema> {
    constructor(private readonly model: Model<SchemaDocument<T>>) { }

    async softDeleteById(id: SchemaId) {
        try {
            const objectId = toObjectId(id);
            const filter: FilterQuery<SchemaDocument<T>> = { _id: objectId } as any;
            const result = await this.model.updateOne(filter, { deletedAt: new Date() });
            if (result.modifiedCount === 0) {
                throw new Error('Document not found or not modified.');
            }
            return result;
        } catch (error) {
            throw error;
        }
    }
}
