import {
    OrderDirection,
} from './constants';

export class CommonListQuery {
    page?: number;

    limit?: number;

    orderBy?: string;

    orderDirection?: OrderDirection;
}

export class CommonListDropdownQuery {
    page?: number;

    limit?: number;

    orderBy?: string;

    orderDirection?: OrderDirection;

    keyword?: string;
}

export interface IGetListResponse<T = any> {
    totalItems: number;
    items: T[];
}


export interface ChapItem {
    id: string;
    name: string;
}