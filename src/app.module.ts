import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './module/user/user.module';
import { StoryModule } from './module/story/story.module';
import { CategoryModule } from './module/category/category.module';
import { MongoModule } from './database/mongo.service';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { ChapModule } from './module/chap/chap.module';
import { UpgradeModule } from './module/upgrade/upgrade.module';
import { QrModule } from './module/qr/qr.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true
    }),
    UserModule,
    StoryModule,
    CategoryModule,
    MongoModule,
    AuthModule,
    ChapModule,
    UpgradeModule,
    QrModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
