import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { UpgradeService } from './upgrade.service';
import { BaseController } from '../../common/base/base.controller';
import { SuccessResponse } from '../../common/response';
import { CreateUpgradeDto, UpdateUpgradeDto } from './upgrade.dto';
import { toObjectId } from '../../common/commonFunctions';

@Controller('upgrade')
export class UpgradeController extends BaseController {
    constructor(
        private readonly upgradeService: UpgradeService,
    ) {
        super()
    }
    @Get()
    async findAll() {
        try {
            const upgrades = await this.upgradeService.getUpgrades();
            return new SuccessResponse(upgrades);
        } catch (error) {
            this.handleError(error);
        }
    }
    
    @Delete(':id')
    async deleteUpgrade(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.upgradeService.deleteUpgrade(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
