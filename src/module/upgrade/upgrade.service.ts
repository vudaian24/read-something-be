import { InjectModel } from '@nestjs/mongoose';
import { Qr, QrDocument } from '../../database/schemas/qr.schema';
import { Upgrade, UpgradeDocument } from '../../database/schemas/upgrade.schema';
import { HttpException, Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { IGetListResponse } from '../../common/interfaces';
import { UserService } from '../user/user.service';
import { toObjectId } from '../../common/commonFunctions';
import { HttpStatus } from '../../common/constants';
import { UserDocument } from '../../database/schemas/user.schema';

@Injectable()
export class UpgradeService {
    constructor(
        @InjectModel(Upgrade.name)
        private readonly upgradeModel: Model<UpgradeDocument>,
    ) { }
    async getUpgrades(): Promise<{
        userIds: string[];
        totalItems: number;
        items: Upgrade[];
    }> {
        try {
            const total = await this.upgradeModel.countDocuments().exec();
            const upgrades = await this.upgradeModel.find().lean().exec();
            const userIds = upgrades.map((item) => item.user_id);
            return {
                userIds: userIds,
                totalItems: total,
                items: upgrades,
            };
        } catch (error) {
            throw error;
        }
    }
    async createUpgrade(dto: {
        user_id: string;
        date: string;
    }) {
        try {
            const existingUpgrade = await this.upgradeModel.findOne({
                user_id: dto.user_id,
                deletedAt: null,
            });
            if (existingUpgrade) {
                throw new HttpException('Người dùng này đã đang đợi nâng cấp tài khoản', HttpStatus.CONFLICT);
            }
            const newUpgrade = new this.upgradeModel(dto);
            return newUpgrade.save();
        }
        catch (error) {
            throw error;
        }
    }
    async updateUpgrade(id: string) {
        try {
            const upgrade = await this.upgradeModel.findOne({
                _id: toObjectId(id),
                deletedAt: null,
            });
            if (!upgrade) {
                throw new HttpException('Không tìm thấy yêu cầu nâng cấp', HttpStatus.BAD_REQUEST);
            }
            return upgrade.user_id;
        } catch (error) {
            throw error;
        }
    }

    async deleteUpgrade(id: Types.ObjectId) {
        try {
            const upgrade = await this.upgradeModel.findById(id);
            if (!upgrade) {
                throw new HttpException('Không tìm thấy yêu cầu nâng cấp', HttpStatus.BAD_REQUEST);
            }
            return await this.upgradeModel.deleteOne(id)
        } catch (error) {
            throw error;
        }
    }

}
