import { Module } from '@nestjs/common';
import { UpgradeController } from './upgrade.controller';
import { UpgradeService } from './upgrade.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Upgrade, UpgradeSchema } from '../../database/schemas/upgrade.schema';
import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';
import { User, UserSchema } from '../../database/schemas/user.schema';
import { StoryService } from '../story/story.service';
import { StoryModule } from '../story/story.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Upgrade.name, schema: UpgradeSchema }]),
  ],
  controllers: [UpgradeController],
  providers: [UpgradeService],
})
export class UpgradeModule { }
