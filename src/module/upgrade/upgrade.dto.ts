export class CreateUpgradeDto {
    user_id: string;
    date: string;
}

export class UpdateUpgradeDto {
    user_id: string;
    date: string;
}