import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { QrService } from './qr.service';
import { BaseController } from '../../common/base/base.controller';
import { SuccessResponse } from '../../common/response';
import { CreateQrDto, UpdateQrDto } from './qr.dto';
import { toObjectId } from '../../common/commonFunctions';

@Controller('qr')
export class QrController extends BaseController {
    constructor(
        private readonly qrService: QrService,
    ) {
        super();
    }
    @Get()
    async findAll() {
        try {
            const qrs = await this.qrService.getQrs();
            return new SuccessResponse(qrs);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post()
    async createQr(
        @Body() dto: CreateQrDto,
    ) {
        try {
            const result = await this.qrService.createQr(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
    @Patch(':id')
    async updateQr(
        @Param('id')
        id: string,
        @Body() dto: UpdateQrDto,
    ) {
        try {
            const result = await this.qrService.updateQr(toObjectId(id), dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
    @Delete(':id')
    async deleteQr(
        @Param('id')
        id: string,
    ) {
        try {
            const result = await this.qrService.deleteQr(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
