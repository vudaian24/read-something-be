import { Module } from '@nestjs/common';
import { QrService } from './qr.service';
import { QrController } from './qr.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Qr, QrSchema } from '../../database/schemas/qr.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Qr.name, schema: QrSchema }]),
  ],
  providers: [QrService],
  controllers: [QrController]
})
export class QrModule {}
