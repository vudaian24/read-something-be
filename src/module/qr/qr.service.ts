import { Qr } from '../../database/schemas/qr.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateQrDto, UpdateQrDto } from './qr.dto';
import { Model, Types } from 'mongoose';

@Injectable()
export class QrService {
    constructor(
        @InjectModel(Qr.name)
        private readonly qrModel: Model<Qr>,
    ) { }
    async getQrs(): Promise<any> {
        try {
            const qr = await this.qrModel.findOne();
            return {
                items: qr,
            };
        } catch (error) {
            throw error;
        }
    }
    async createQr(dto: CreateQrDto) {
        try {
            const qr: SchemaCreateDocument<Qr> = {
                ...(dto as any)
            }
            const newQr = await this.qrModel.create(qr);
            return newQr;
        } catch (error) {
            throw error;
        }
    }
    async updateQr(id: Types.ObjectId, dto: UpdateQrDto) {
        try {
            const updateQr = await this.qrModel.findByIdAndUpdate(id, dto, { new: true });
            return updateQr;
        } catch (error) {
            throw error;
        }
    }
    async deleteQr(id: Types.ObjectId) {
        try {
            const deleteQr = await this.qrModel.findByIdAndDelete(id);
            return deleteQr;
        } catch (error) {
            throw error;
        }
    }
}
