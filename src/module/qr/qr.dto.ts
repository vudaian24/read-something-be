export class CreateQrDto {
    image: string;
}

export class UpdateQrDto {
    image: string;
}