import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { BaseController } from '../../common/base/base.controller';
import { CategoryService } from './category.service';
import { SuccessResponse } from '../../common/response';
import { CreateCategoryDto, UpdateCategoryDto } from './dto/category.dto';
import { toObjectId } from '../../common/commonFunctions';

@Controller('category')
export class CategoryController extends BaseController {
    constructor(private readonly categoryService: CategoryService) {
        super();
    }

    @Get()
    async findAll() {
        try {
            const categories = await this.categoryService.getCategories();
            return new SuccessResponse(categories);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post()
    async createCategory(
        @Body() dto: CreateCategoryDto,
    ) {
        try {
            const result = await this.categoryService.createCategory(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateCategory(
        @Param('id') id: string,
        @Body() dto: UpdateCategoryDto,
    ) {
        try {
            const result = await this.categoryService.updateCategory(toObjectId(id), dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get(':id')
    async getCategoryDetail(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.categoryService.findCategoryById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Delete(':id')
    async deleteCategory(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.categoryService.deleteCategory(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
