import { Category } from "../../database/schemas/category.schema"

export enum CategoryOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
    UPDATED_AT = 'updatedAt',
}

export const CategoryAttributesForList: (keyof Category)[] = [
    "_id",
    "name",
    "description",
    'createdAt',
    'updatedAt',
]

export const CategoryAttributesForDetail: (keyof Category)[] = [
    "_id",
    "name",
    "description",
    'createdAt',
    'updatedAt',
]