import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Category, CategoryDocument } from '../../database/schemas/category.schema';
import { CreateCategoryDto, UpdateCategoryDto } from './dto/category.dto';

@Injectable()
export class CategoryService {
    constructor(
        @InjectModel(Category.name)
        private readonly categoryModel: Model<CategoryDocument>
    ) { }

    async getCategories() {
        try {
            const categories = await this.categoryModel.find();
            return categories;
        } catch (error) {
            throw error;
        }
    }

    async createCategory(dto: CreateCategoryDto) {
        try {
            const newCategory = await this.categoryModel.create(dto);
            return newCategory;
        } catch (error) {
            throw error;
        }
    }

    async updateCategory(id: Types.ObjectId, dto: UpdateCategoryDto) {
        try {
            const existingCategory = await this.categoryModel.findById(id);
            if (!existingCategory) {
                throw new HttpException('Không tìm thấy thể loại', HttpStatus.NOT_FOUND);
            }
            const updatedCategory = await this.categoryModel.findByIdAndUpdate(id, dto, { new: true });
            return updatedCategory;
        } catch (error) {
            throw error;
        }
    }

    async findCategoryById(id: Types.ObjectId) {
        try {
            const category = await this.categoryModel.findById(id);
            if (!category) {
                throw new HttpException('Không tìm thấy thể loại', HttpStatus.NOT_FOUND);
            }
            return category;
        } catch (error) {
            throw error;
        }
    }

    async deleteCategory(id: Types.ObjectId) {
        try {
            const category = await this.categoryModel.findById(id);
            if (!category) {
                throw new HttpException('Không tìm thấy thể loại', HttpStatus.NOT_FOUND);
            }
            const deletedCategory = await this.categoryModel.findByIdAndDelete(id);
            return deletedCategory;
        } catch (error) {
            throw error;
        }
    }
}
