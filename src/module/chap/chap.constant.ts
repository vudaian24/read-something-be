import { Chap } from "../../database/schemas/chap.schema";

export enum ChapOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
    UPDATED_AT = 'updatedAt',
}

export const ChapAttributesForList: (keyof Chap)[] = [
    "_id",
    "name",
    "description",
    "images",
    'createdAt',
    'updatedAt',
]

export const ChapAttributesForDetail: (keyof Chap)[] = [
    "_id",
    "name",
    "description",
    "images",
    'createdAt',
    'updatedAt',
]