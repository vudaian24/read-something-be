import { Module } from '@nestjs/common';
import { ChapController } from './chap.controller';
import { ChapService } from './chap.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Chap, ChapSchema } from '../../database/schemas/chap.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Chap.name, schema: ChapSchema }]),
  ],
  controllers: [ChapController],
  providers: [ChapService]
})
export class ChapModule { }
