import { ChapService } from '../chap/chap.service';
import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { BaseController } from '../../common/base/base.controller';
import { toObjectId } from '../../common/commonFunctions';
import { SuccessResponse } from '../../common/response';
import { CreateChapDto, UpdateChapDto } from '../chap/dto/chap.dto';
@Controller('chap')
export class ChapController extends BaseController{
    constructor(private readonly chapService: ChapService) {
        super();
    }

    @Get()
    async findAll() {
        try {
            const chaps = await this.chapService.getChaps();
            return new SuccessResponse(chaps);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateChap(
        @Param('id')
        id: string,
        @Body()
        dto: UpdateChapDto,
    ) {
        try {
            const res = await this.chapService.updateChap(toObjectId(id), dto);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }
    @Get(':id')
    async getChapDetail(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.chapService.findById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
