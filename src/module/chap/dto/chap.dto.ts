export class CreateChapDto {
    storyId: string;
    name: string;
    description?: string;
    images?: string[];
}

export class UpdateChapDto {
    chapId: string;
    name: string;
    description?: string;
    images?: string[];
}