import { IGetListResponse } from '../../common/interfaces';
import { Chap, ChapDocument } from '../../database/schemas/chap.schema';
import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateChapDto, UpdateChapDto } from './dto/chap.dto';
import { HttpStatus } from '../../common/constants';
import { ChapAttributesForDetail } from './chap.constant';
import { toObjectId } from '../../common/commonFunctions';
import { StoryService } from '../story/story.service';

@Injectable()
export class ChapService {
    constructor(
        @InjectModel(Chap.name)
        private readonly chapModel: Model<ChapDocument>,
    ) { }

    async getChaps(): Promise<IGetListResponse<Chap>> {
        try {
            const total = await this.chapModel.countDocuments();
            const story = await this.chapModel.find();
            return {
                totalItems: total,
                items: story || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async createChap(dto: any) {
        try {
            const chap: SchemaCreateDocument<Chap> = {
               ...(dto as any)
            }
            const createChap = this.chapModel.create(chap);
            return createChap;
        } catch (error) {
            throw error;
        }
    }
    async findById(
        id: Types.ObjectId,
        attributes: (keyof Chap)[] = ChapAttributesForDetail,
    ) {
        try {
            const res = await this.chapModel.findById(id, attributes);

            return res;
        } catch (error) {
            throw error;
        }
    }

    async updateChap(id: Types.ObjectId, dto: UpdateChapDto) {
        try {
            const updateChap = await this.chapModel.findByIdAndUpdate(id, dto, { new: true });
            return updateChap;
        } catch (error) {
            throw error;
        }
    }

    async deleteChap(id: Types.ObjectId) {
        try {
            const existing = await this.chapModel.findOne({
                _id: id,
                deletedAt: null,
            });
            if (!existing) {
                throw new HttpException('Không tìm thấy tập', HttpStatus.BAD_REQUEST);
            }
            const updateChap = await this.chapModel.deleteOne(id);
            return updateChap;
        } catch (error) {
            throw error;
        }
    }
}
