import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { BaseController } from '../../common/base/base.controller';
import { UserService } from './user.service';
import { ErrorResponse, SuccessResponse } from '../../common/response';
import { CreateUserDto, UpdateUserDto } from './dto/user.dto';
import { toObjectId } from '../../common/commonFunctions';
import { Types } from 'mongoose';
import { CreateUpgradeDto } from '../upgrade/upgrade.dto';
import { CreateStoryDto } from '../story/dto/story.dto';
import { CreateChapDto, UpdateChapDto } from '../chap/dto/chap.dto';
import { HttpStatus } from '../../common/constants';

@Controller('user')
export class UserController extends BaseController {
    constructor(private readonly userService: UserService) {
        super();
    }

    @Get()
    async findAll() {
        try {
            const users = await this.userService.getUsers();
            return new SuccessResponse(users);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post()
    async createUser(
        @Body() dto: CreateUserDto,
    ) {
        try {
            const result = await this.userService.createUser(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('/follow')
    async updateFollow(
        @Body() dto: { idUser: Types.ObjectId, idStory: Types.ObjectId },
    ) {
        try {
            const result = await this.userService.updateFollow(dto.idUser, dto.idStory);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
    @Post('/story/add')
    async createStory(
        @Body() dto: CreateStoryDto
    ) {
        try {
           const res = await this.userService.createStory(dto);
           return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateUser(
        @Param('id')
        id: string,
        @Body()
        dto: UpdateUserDto,
    ) {
        try {
            const result = await this.userService.updateUser(toObjectId(id), dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get(':id')
    async getUserDetail(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.userService.findUserById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Delete(':id')
    async deleteUser(
        @Param('id')
        id: string,
    ) {
        try {
            const result = await this.userService.deleteUser(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('/upgrade/all')
    async getAllUpgreade(
        @Body() dto: { idUser: Types.ObjectId },
    ) {
        try {
            const result = await this.userService.getAllUpgrades();
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('/upgrade/accept')
    async updateUpgrade(
        @Body() id: {
            id: string;
        },
    ) {
        try {
            const result = await this.userService.updateUpgrade(id.id);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('/upgrade/send')
    async createUpgrade(
        @Body() dto: CreateUpgradeDto,
    ) {
        try {
            if(dto.user_id === "" || dto.user_id === undefined) {
                const errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, 'Không có userId truyền vào');
                return errorResponse;
            } else {
                const result = await this.userService.createUpgrade(dto);
                return result;
            }
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('/story/list')
    async rejectUpgrade(
        @Body() id: {
            userId: string;
        }
    ) {
        try {
            const res = await this.userService.storyList(id.userId);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('/chap')
    async createChap(
        @Body() dto: CreateChapDto
    ) {
        try {
            const result = await this.userService.createChap(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    
    @Post('/chap/delete')
    async deleteChap(
        @Body() dto: {
            storyId: string,
            chapId: string,
        },
    ) {
        try {
            const res = await this.userService.deleteChap(dto);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('/story/delete')
    async deleteStory(
        @Body() dto: {
            id: string;
        }
    ) {
        try {
            const result = await this.userService.deleteStory(toObjectId(dto.id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
