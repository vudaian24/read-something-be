export class CreateUserDto {
    username: string;
    password: string;
    avatar: string;
    follow: string[];
    role: string;
}

export class UpdateUserDto {
    username: string;
    password: string;
    avatar: string;
    follow: string[];
    role: string;
}