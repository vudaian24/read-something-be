import { HttpException, Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../../database/schemas/user.schema';
import { HttpStatus } from '../../common/constants';
import { UserAttributesForDetail, UserAttributesForList } from './user.constant';
import { CreateUserDto, UpdateUserDto } from './dto/user.dto';
import { IGetListResponse } from '../../common/interfaces';
import { StoryService } from '../story/story.service';
import { toObjectId } from '../../common/commonFunctions';
import { CreateStoryDto } from '../story/dto/story.dto';
import { UpgradeService } from './../upgrade/upgrade.service';
import { CreateChapDto, UpdateChapDto } from '../chap/dto/chap.dto';
import { ChapService } from './../chap/chap.service';
import * as bcrypt from 'bcrypt';
import { ErrorResponse, SuccessResponse } from '../../common/response';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(User.name)
        private readonly userModel: Model<UserDocument>,
        private readonly storyService: StoryService,
        private readonly upgradeService: UpgradeService,
        private readonly chapService: ChapService,
    ) { }

    async getAllUpgrades() {
        try {
            const res = await this.upgradeService.getUpgrades();
    
            if (!res) {
                throw new Error('No response from upgradeService');
            }
    
            const userIds = res.userIds || [];
            if (userIds.length === 0) {
                return { totalItems: 0, items: [] };
            }
    
            const users = await Promise.all(userIds.map(async (id) => {
                try {
                    return await this.userModel.findOne(toObjectId(id));
                } catch (error) {
                    console.error(`Error fetching user with ID ${id}:`, error);
                    return null;
                }
            }));
    
            const userMap = new Map(users.filter(user => user).map(user => [user._id.toString(), user]));
    
            const upgradesWithUsers = res.items.map(upgrade => {
                const userIdStr = upgrade.user_id ? upgrade.user_id.toString() : null;
                return {
                    ...upgrade,
                    user: userMap.get(userIdStr) || null,
                };
            });
    
            const total = res.totalItems || 0;
    
            return {
                totalItems: total,
                items: upgradesWithUsers,
            };
        } catch (error) {
            console.error('Error in getAllUpgrades:', error);
            throw error;
        }
    }        

    async createUpgrade(dto: {
        user_id: string;
        date: string;
    }) {
        try {
            const checkUser = await this.userModel.findOne({ _id: toObjectId(dto.user_id), deletedAt: null });
            if(!checkUser) {
                return new ErrorResponse(HttpStatus.BAD_REQUEST, 'Không tìm thấy người dùng nào có id này');
            }
            const existingUser = await this.userModel.findOne({
                _id: toObjectId(dto.user_id),
                deletedAt: null,
                role: 'author'
            });
            if (existingUser) {
                return new ErrorResponse(HttpStatus.BAD_REQUEST, 'Người dùng này đã có quyền tác giả');
            }
            const upgrade = await this.upgradeService.createUpgrade(dto);
            return new SuccessResponse(upgrade);
        } catch (error) {
            throw error;
        }
    }

    async updateUpgrade(id: string) {
        try {
            const upgrade = await this.upgradeService.updateUpgrade(id);

            const upgradeObjectId = toObjectId(upgrade);

            const idObjectId = toObjectId(id);
            await this.userModel.updateOne({ _id: upgradeObjectId }, { role: 'author' });
            await this.upgradeService.deleteUpgrade(idObjectId);
            return upgrade;
        } catch (error) {
            throw error;
        }
    }

    async getUsers(): Promise<IGetListResponse<User>> {
        try {
            const users = await this.userModel.find({
                deletedAt: null,
                role: { $in: ['user', 'author'] }
            });
            return {
                totalItems: users.length,
                items: users || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async createUser(dto: CreateUserDto) {
        try {
            const existingUser = await this.userModel.findOne({
                username: dto.username,
                deletedAt: null,
            });

            if (existingUser) {
                throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
            }
            const user: SchemaCreateDocument<User> = {
                ...(dto as any),
            };

            const createUser = this.userModel.create(user);
            return createUser;
        } catch (error) {
            throw error;
        }
    }

    async updateUser(id: Types.ObjectId, dto: UpdateUserDto) {
        try {
            const existingUser = await this.userModel.findOne({
                _id: id,
                deletedAt: null,
            });

            if (!existingUser) {
                throw new HttpException('Không tìm thấy người dùng', HttpStatus.BAD_REQUEST);
            }

            const updateData: Partial<UpdateUserDto> = {};

            if (dto.password && dto.password !== "") {
                updateData.password = await bcrypt.hash(dto.password, 10);
            }

            if (dto.avatar && dto.avatar !== "") {
                updateData.avatar = dto.avatar;
            }

            if (dto.username && dto.username !== "") {
                updateData.username = dto.username;
            }

            if (dto.follow && dto.follow.length > 0) {
                updateData.follow = dto.follow;
            }

            if (dto.role && dto.role !== "") {
                updateData.role = dto.role;
            }

            const updatedUser = await this.userModel.findByIdAndUpdate(id, updateData, { new: true });

            return updatedUser;
        } catch (error) {
            throw error;
        }
    }

    async updateFollow(idUser: Types.ObjectId, idStory: Types.ObjectId) {
        try {
            const existingUser = await this.userModel.findOne({
                _id: idUser,
                deletedAt: null,
            });
            if (!existingUser) {
                throw new HttpException('Không tìm thấy người dùng', HttpStatus.BAD_REQUEST);
            }

            const updateUser = await this.userModel.findByIdAndUpdate(idUser, { $push: { follow: idStory } }, { new: true });
            return updateUser;
        } catch (err) {
            throw err;
        }
    }

    async findUserById(
        id: Types.ObjectId,
        attributes: (keyof User)[] = UserAttributesForDetail,
    ) {
        try {
            const user = await this.userModel.findById(id, attributes);
            const follows = user.follow;
            const followPromises = follows.map((item) => this.storyService.checkFollow(item));
            const followResults = await Promise.all(followPromises);
            const userFull = {
                ...user.toObject(),
                follow: followResults
            };
            return userFull;
        } catch (error) {
            throw error;
        }
    }

    async deleteUser(id: Types.ObjectId) {
        try {
            const existingUser = await this.userModel.findOne({
                _id: id,
                deletedAt: null,
            });
            if (!existingUser) {
                throw new HttpException('Không tìm thấy người dùng', HttpStatus.BAD_REQUEST);
            }
            const deletedUser = await this.userModel.deleteOne(id);
            return deletedUser;
        } catch (error) {
            throw error;
        }
    }

    async createStory(dto: CreateStoryDto) {
        try {
            const existingUser = await this.userModel.findOne({
                _id: toObjectId(dto.userId),
                deletedAt: null,
            });

            if (!existingUser) {
                return {
                    message: 'Người dùng này không phải là tác giả',
                    status: HttpStatus.BAD_REQUEST
                }
            }
            const author = dto.author && dto.author !== '' ? dto.author : existingUser.username;
            const story = await this.storyService.createStory({
                title: dto.title,
                author: author,
                category: dto.category,
                description: dto.description,
                follow: dto.follow,
                status: dto.status,
                chap: dto.chap,
                image: dto.image,
            });

            await this.userModel.updateOne(
                { _id: toObjectId(dto.userId) },
                { $push: { story: story.data._id } }
            );

            return story;
        } catch (error) {
            throw error;
        }
    }

    async storyList(userId: string) {
        try {
            const existingUser = await this.userModel.findOne({
                _id: toObjectId(userId),
                deletedAt: null,
            });
            if (!existingUser) {
                throw new HttpException('Không tìm thấy tác giả', HttpStatus.BAD_REQUEST);
            }
            const storyIds = existingUser.story.map((item) => item);
            const stories = await Promise.all(storyIds.map((id) => this.storyService.findById(toObjectId(id))));
            return {
                totalItems: stories.length || 0,
                items: stories || [],
            }
        } catch (error) {
            throw error;
        }
    }

    async createChap(dto: CreateChapDto) {
        try {
            const storyId = toObjectId(dto.storyId);

            const chap = await this.chapService.createChap({
                name: dto.name,
                description: dto.description,
                images: dto.images,
            });
            const update = {
                $push: {
                    chap: {
                        id: chap._id,
                        name: chap.name,
                    },
                },
            } as any;
            await this.storyService.updateStory(storyId, update);
            return chap;
        } catch (error) {
            throw error;
        }
    }

    async deleteChap(dto: {
        storyId: string,
        chapId: string,
    }) {
        try {
            const chapId = toObjectId(dto.chapId);
            const storyId = toObjectId(dto.storyId)
            const story = await this.storyService.findById(storyId);
            if (!story) {
                throw new HttpException('Không tìm thấy truyện chứa chap này', HttpStatus.BAD_REQUEST);
            }
            const update = {
                $pull: {
                    chap: { id: chapId },
                },
            } as any;
            await this.storyService.updateStory(toObjectId(story._id), update);
            await this.chapService.deleteChap(chapId);

            return 'Xóa thành công chap';
        } catch (error) {
            throw error;
        }
    }

    async deleteStory(id: Types.ObjectId) {
        try {
            const story = await this.storyService.findById(id);
            if (!story) {
                throw new HttpException('Không tìm thấy truyện này', HttpStatus.BAD_REQUEST);
            }
            await this.storyService.deleteStory(id);
            const author = await this.userModel.findOne({
                username: story.author,
            })
            await this.userModel.findByIdAndUpdate(
                author._id,
                { $pull: { story: id } },
                { new: true }
            );

            return 'Xóa thành công truyện';
        } catch (error) {
            throw error;
        }
    }
}
