import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../../database/schemas/user.schema';
import { StoryService } from '../story/story.service';
import { Story, StorySchema } from '../../database/schemas/story.schema';
import { UpgradeService } from '../upgrade/upgrade.service';
import { UpgradeModule } from '../upgrade/upgrade.module';
import { Upgrade, UpgradeSchema } from '../../database/schemas/upgrade.schema';
import { Chap, ChapSchema } from '../../database/schemas/chap.schema';
import { ChapService } from '../chap/chap.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([{ name: Story.name, schema: StorySchema }]),
    MongooseModule.forFeature([{ name: Upgrade.name, schema: UpgradeSchema }]),
    MongooseModule.forFeature([{ name: Chap.name, schema: ChapSchema }]),
  ],
  controllers: [UserController],
  providers: [UserService, StoryService, UpgradeService, ChapService],
})
export class UserModule { }
