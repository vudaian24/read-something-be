import { BaseController } from '../../common/base/base.controller';
import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { StoryService } from './story.service';
import { SuccessResponse } from '../../common/response';
import { CreateStoryDto, UpdateStoryDto } from './dto/story.dto';
import { toObjectId } from '../../common/commonFunctions';

@Controller('story')
export class StoryController extends BaseController {
    constructor(private readonly storyService: StoryService) {
        super();
    }

    @Get()
    async findAll() {
        try {
            const storys = await this.storyService.getStorys();
            return new SuccessResponse(storys);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get('/rank/all')
    async getRankAll() {
        try {
            const res = await this.storyService.getRankAll();
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get('/rank/week')
    async getRankWeek() {
        try {
            const res = await this.storyService.getRankWeek();
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get('/rank/day')
    async getRankDay() {
        try {
            const res = await this.storyService.getRankDay();
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('search')
    async searchStory(@Body() query: any): Promise<any> {
        try {
            const res = await this.storyService.searchStory(query);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateStory(
        @Param('id')
        id: string,
        @Body()
        dto: UpdateStoryDto,
    ) {
        try {
            const res = await this.storyService.updateStory(toObjectId(id), dto);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get(':id')
    async getUserDetail(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.storyService.findById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Delete(':id')
    async deleteStory(
        @Param('id') id: string,
    ) {
        try {
            const res = await this.storyService.deleteStory(toObjectId(id));
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('/view')
    async addView(
        @Body() storyId: {
            id: string
        }
    ) {
        try {
            const res = await this.storyService.addView(toObjectId(storyId.id));
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }
}
