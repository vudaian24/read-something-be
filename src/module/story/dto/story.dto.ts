import { ChapItem } from "../../../common/interfaces";

export class CreateStoryDto {
    userId: string;
    title: string;
    author: string;
    category: string;
    description: string;
    follow: string[];
    status: string;
    chap: ChapItem[];
    image: string;
}



export class UpdateStoryDto {
    title: string;
    author: string;
    category: string;
    description: string;
    follow: string[];
    status: string;
    chap: ChapItem[];
    image: string;
}