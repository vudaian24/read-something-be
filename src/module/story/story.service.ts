import { InjectModel } from '@nestjs/mongoose';
import { IGetListResponse } from '../../common/interfaces';
import { Story, StoryDocument } from '../../database/schemas/story.schema';
import { HttpException, Injectable } from '@nestjs/common';
import { FilterQuery, Model, Types } from 'mongoose';
import { CreateStoryDto, UpdateStoryDto } from './dto/story.dto';
import { HttpStatus } from '../../common/constants';
import { StoryAttributesForDetail } from './story.constant';
import { startOfWeek, endOfWeek, startOfDay, endOfDay } from 'date-fns';

@Injectable()
export class StoryService {
    constructor(
        @InjectModel(Story.name)
        private readonly storyModel: Model<StoryDocument>,
    ) { }

    async getStorys(): Promise<IGetListResponse<Story>> {
        try {
            const total = await this.storyModel.countDocuments();
            const story = await this.storyModel.find();
            return {
                totalItems: total,
                items: story || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async searchStory(query: any): Promise<any> {
        try {
            const regexQuery = {
                title: { $regex: query.title, $options: 'i' },
            };
            const res = await this.storyModel.find(regexQuery);
            return res;
        } catch (error) {
            throw error;
        }
    }


    async createStory(dto: any) {
        try {
            const newStory = new this.storyModel({
                title: dto.title,
                author: dto.author,
                category: dto.category,
                description: dto.description,
                follow: dto.follow,
                status: dto.status,
                chap: dto.chap,
                image: dto.image,
                totalView: 0,
            })
            const res = await newStory.save();
            return {
                message: 'Tạo truyện thành công',
                data: newStory,
            };
        } catch (error) {
            throw error;
        }
    }

    async updateStory(id: Types.ObjectId, dto: UpdateStoryDto) {
        try {
            const existingStory = await this.storyModel.findOne({
                _id: id,
                deletedAt: null,
            })
            if (!existingStory) {
                throw new HttpException('Không tìm thấy truyện', HttpStatus.BAD_REQUEST);
            }
            const updateStory = await this.storyModel.findByIdAndUpdate(
                id,
                dto,
                { new: true }
            )
            return updateStory;
        } catch (error) {
            throw error;
        }
    }

    async findById(
        id: Types.ObjectId,
        attributes: (keyof Story)[] = StoryAttributesForDetail,
    ) {
        try {
            const existingStory = await this.storyModel.findOne({
                _id: id,
                deletedAt: null,
            })
            if (!existingStory) {
                throw new HttpException('Không tìm thấy truyện', HttpStatus.BAD_REQUEST);
            }
            return await this.storyModel.findById(id, attributes);
        } catch (error) {
            throw error;
        }
    }

    async checkFollow(id: string) {
        const story = await this.storyModel.findOne({
            _id: id,
            deletedAt: null,
        })
        return story
    }

    async deleteStory(id: Types.ObjectId) {
        try {
            const existingStory = await this.storyModel.findOne({
                _id: id,
                deletedAt: null,
            })
            if (!existingStory) {
                throw new HttpException('Không tìm thấy truyện', HttpStatus.BAD_REQUEST);
            }
            const deleteStory = await this.storyModel.deleteOne(id);
            return deleteStory;
        } catch (error) {
            throw error;
        }
    }

    async addView(id: Types.ObjectId) {
        try {
            const existingStory = await this.storyModel.findOne({
                _id: id,
                deletedAt: null,
            })
            if (!existingStory) {
                throw new HttpException('Không tìm thấy truyện', HttpStatus.BAD_REQUEST);
            }
            const updateStory = await this.storyModel.findByIdAndUpdate(
                id,
                { totalView: existingStory.totalView + 1 },
                { new: true }
            )
            return updateStory;
        } catch (error) {
            throw error;
        }
    }

    //Rank

    async getRankAll() {
        try {
            const story = await this.storyModel.find()
                .sort({ totalView: -1 })
                .limit(10);
            return {
                items: story || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async getRankWeek() {
        try {
            const now = new Date();
            const startOfWeekDate = startOfWeek(now, { weekStartsOn: 1 });
            const endOfWeekDate = endOfWeek(now, { weekStartsOn: 1 });

            const story = await this.storyModel.find({
                updatedAt: {
                    $gte: startOfWeekDate.getTime(),
                    $lt: endOfWeekDate.getTime(),
                },
            })
                .sort({ totalView: -1 })
                .limit(10);

            return {
                items: story || [],
            };
        } catch (error) {
            throw error;
        }
    }
    async getRankDay() {
        try {
            const now = new Date();
            const startOfDayDate = startOfDay(now);
            const endOfDayDate = endOfDay(now);


            const stories = await this.storyModel.find({
                updatedAt: {
                    $gte: startOfDayDate.getTime(),
                    $lt: endOfDayDate.getTime(),
                },
            })
                .sort({ totalView: -1 })
                .limit(10);

            return {
                items: stories || [],
            };
        } catch (error) {
            throw error;
        }
    }
}
