import { Story } from "../../database/schemas/story.schema";

export enum StoryOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
    UPDATED_AT = 'updatedAt',
}

export const StoryAttributesForList: (keyof Story)[] = [
    "title",
    "image",
    "category",
    'author',
    'follow',
    'description',
    'totalView',
    'status',
    'chap',
    'createdAt',
    'updatedAt',
]

export const StoryAttributesForDetail: (keyof Story)[] = [
    "title",
    "image",
    "category",
    'author',
    'follow',
    'totalView',
    'description',
    'status',
    'chap',
    'createdAt',
    'updatedAt',
]