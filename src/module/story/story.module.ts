import { Module } from '@nestjs/common';
import { StoryController } from './story.controller';
import { StoryService } from './story.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Story, StorySchema } from '../../database/schemas/story.schema';
import { UserService } from '../user/user.service';
import { User, UserSchema } from '../../database/schemas/user.schema';
import { ChapService } from '../chap/chap.service';
import { Chap, ChapSchema } from '../../database/schemas/chap.schema';
import { ChapModule } from '../chap/chap.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Story.name, schema: StorySchema }]),
  ],  
  controllers: [StoryController],
  providers: [StoryService]
})
export class StoryModule {}
