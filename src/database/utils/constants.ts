export enum MongoCollection {
    USERS = 'users',
    STORY = 'stories',
    UPGREADE = 'upgrades',
    CATEGORY = 'categorys',
    CHAP = 'chapters',
    QR = 'qrs',
}
