import { Prop, Schema,SchemaFactory } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';
import { ChapItem } from '../../common/interfaces';
export type StoryDocument = SchemaDocument<Story>;
@Schema({
    timestamps: true,
    collection: MongoCollection.STORY,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})


export class Story extends MongoBaseSchema {
    @Prop({ required: false })
    title: string;

    @Prop({ required: false })
    author: string;

    @Prop({ required: false })
    category: string;

    @Prop({ required: false })
    description: string;

    @Prop({ required: false })
    follow: string[];

    @Prop({  default: 0 })
    totalView: number;

    @Prop({ required: false })
    status: string;

    @Prop({ required: false })
    chap: ChapItem[];

    @Prop({ required: false, default: 'image' })
    image: string;
}   

const StorySchema = SchemaFactory.createForClass(Story);

export { StorySchema };