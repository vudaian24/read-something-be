import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';
export type CategoryDocument = SchemaDocument<Category>;
@Schema({
    timestamps: true,
    collection: MongoCollection.CATEGORY,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Category extends MongoBaseSchema {
    @Prop({ required: false })
    name: string;

    @Prop({ required: false })
    description: string;
}

const CategorySchema = createSchemaForClass(Category);

export { CategorySchema };