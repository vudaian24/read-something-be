import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';
export type UpgradeDocument = SchemaDocument<Upgrade>;

@Schema({
    timestamps: true,
    collection: MongoCollection.UPGREADE,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Upgrade extends MongoBaseSchema {
    @Prop({ required: false })
    user_id: string;

    @Prop({ required: false })
    date: string;
}

const UpgradeSchema = createSchemaForClass(Upgrade);

export { UpgradeSchema };