import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';

export type QrDocument = SchemaDocument<Qr>;

@Schema({
    timestamps: true,
    collection: MongoCollection.QR,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    }
})

export class Qr extends MongoBaseSchema {
    @Prop({ required: false })
    image: string;
}

const QrSchema = createSchemaForClass(Qr);
export { QrSchema }