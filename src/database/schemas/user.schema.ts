import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';
export type UserDocument = SchemaDocument<User>;
@Schema({
    timestamps: true,
    collection: MongoCollection.USERS,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})
export class User extends MongoBaseSchema {
    @Prop({ required: false })
    username: string;

    @Prop({ required: false })
    password: string;

    @Prop({ type: [String], default: [] })
    story: string[];

    @Prop({ type: [String], default: [] })
    follow: string[];
    
    @Prop({ required: false })
    avatar: string;

    @Prop()
    role?: string;
}

const UserSchema = createSchemaForClass(User);

export { UserSchema };
