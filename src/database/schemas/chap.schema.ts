import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';
export type ChapDocument = SchemaDocument<Chap>;
@Schema({
    timestamps: true,
    collection: MongoCollection.CHAP,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Chap extends MongoBaseSchema {
    @Prop({ required: false })
    name: string;

    @Prop({ required: false })
    description: string;

    @Prop({ required: false })
    images: string[];
}

const ChapSchema = createSchemaForClass(Chap);

export { ChapSchema };