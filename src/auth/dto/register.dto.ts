export class RegisterDto {
    username: string;
    password: string;
    avatar: string;
    role: string;
}