import { Body, Controller, Get, Post, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { SuccessResponse } from '../common/response';
import { BaseController } from '../common/base/base.controller';
import { Request } from 'express';
import { RegisterDto } from './dto/register.dto';

@Controller('auth')
export class AuthController extends BaseController {
    constructor(private authService: AuthService) { super() }
    @Post('login')
    async login(@Body() loginDto: LoginDto) {
        try {
            const res = await this.authService.login(loginDto);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('register')
    async register(@Body() registerDto: RegisterDto) {
        try {
            const res = await this.authService.register(registerDto);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }


    @Get('get-user')
    async getUser(@Req() req: Request) {
        try {
            const res = await this.authService.getUser(req);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }
}
